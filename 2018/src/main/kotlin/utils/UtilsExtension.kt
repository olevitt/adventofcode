package utils

fun String.asResource(work: (String) -> Unit) {
    val content = this.javaClass::class.java.getResource(this).readText()
    work(content)
}

fun String.asResourceText() : String {
    val content = this.javaClass::class.java.getResource(this).readText()
    return content;
}