package utils

interface Day {
    fun step1() : String
    fun step2() : String
}