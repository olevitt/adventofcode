import utils.Day
import utils.asResourceText
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.util.*


class Day5 : Day {

    override fun step1() : String {
        val result = epure("/day5.txt".asResourceText())
        return "" + result.length
    }

    override fun step2(): String {
        var charToDelete = 'a'
        val input = "/day5.txt".asResourceText()
        var min = Integer.MAX_VALUE
        while (charToDelete <= 'z') {
            var toTest = input.replace(charToDelete+"","")
            toTest = toTest.replace(charToDelete.toUpperCase()+"","")
            min = Math.min(epure(toTest).length, min)
            charToDelete++
        }
        return ""+min
    }

    fun epure(str : String) : String {
        val chars = str.toCharArray().toMutableList()
        var i = 0
        while (i < chars.size-1) {
            if (Math.abs(chars[i] - chars[i+1]) == 32) {
                chars.removeAt(i)
                chars.removeAt(i)
                if (i != 0) {
                    i--
                }
            }
            else {
                i++
            }
        }

        return String(chars.toCharArray())
    }


}