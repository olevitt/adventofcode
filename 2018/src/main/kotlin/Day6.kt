import utils.Day
import utils.asResourceText
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.util.*


class Day6 : Day {

    val extractPosition = Regex("(.*), (.*)")
    var maxId = 1

    override fun step1() : String {
        val points = mutableListOf<Point>()
        var maxX = 0
        var maxY = 0
        "/day6.txt".asResourceText().lines().forEach {
            val values = extractPosition.find(it)!!.groupValues;
            points.add(Point(Integer.parseInt(values[1]), Integer.parseInt(values[2]), maxId++))
            maxX = Math.max(maxX,Integer.parseInt(values[1]))
            maxY = Math.max(maxY,Integer.parseInt(values[2]))
        }

        val board = hashMapOf<String, Int>()
        val casesCouvertesParChaquePoint = hashMapOf<Int,Int>()
        for (i in 0 until maxX) {
            for (j in 0 until maxY) {
                var minDistance = Integer.MAX_VALUE
                var egalite = false
                var point : Point? = null
                points.forEach {
                    val distance = manhattanDistance(it, i, j)
                    if (distance < minDistance) {
                        minDistance = distance
                        point = it
                        egalite = false
                    }
                    else if (distance == minDistance){
                        egalite = true
                    }
                }

                if (i == 0 || j == 0 || i == maxX - 1 || j == maxY - 1) {
                    casesCouvertesParChaquePoint[point!!.id] = -1 * Integer.MAX_VALUE
                }

                if (minDistance != 0) {
                    if (!egalite) {
                        casesCouvertesParChaquePoint[point!!.id] = 1 + casesCouvertesParChaquePoint.getOrDefault(point!!.id,0)
                        board[""+i+"/"+j] = point!!.id
                    }
                    else {
                        board[""+i+"/"+j] = 0
                    }
                }
            }
        }

        print(casesCouvertesParChaquePoint)

        return ""+(casesCouvertesParChaquePoint.maxBy { it.value }!!.value + 1)

    }

    fun manhattanDistance(point : Point, x : Int, y : Int) : Int {
        return Math.abs(point.x - x) + Math.abs(point.y - y)
    }

    override fun step2(): String {
        val points = mutableListOf<Point>()
        var maxX = 0
        var maxY = 0
        "/day6.txt".asResourceText().lines().forEach {
            val values = extractPosition.find(it)!!.groupValues;
            points.add(Point(Integer.parseInt(values[1]), Integer.parseInt(values[2]), maxId++))
            maxX = Math.max(maxX,Integer.parseInt(values[1]))
            maxY = Math.max(maxY,Integer.parseInt(values[2]))
        }

        val board = hashMapOf<String, Int>()
        val casesCouvertesParChaquePoint = hashMapOf<Int,Int>()
        var nbSafes = 0
        for (i in 0 until maxX) {
            for (j in 0 until maxY) {
                var totalDistance = 0
                var minDistance = Integer.MAX_VALUE
                var egalite = false
                var point : Point? = null
                points.forEach {
                    val distance = manhattanDistance(it, i, j)
                    totalDistance += distance
                    if (distance < minDistance) {
                        minDistance = distance
                        point = it
                        egalite = false
                    }
                    else if (distance == minDistance){
                        egalite = true
                    }
                }

                if (totalDistance < 10000) {
                    nbSafes++
                }

                if (i == 0 || j == 0 || i == maxX - 1 || j == maxY - 1) {
                    casesCouvertesParChaquePoint[point!!.id] = -1 * Integer.MAX_VALUE
                }

                if (minDistance != 0) {
                    if (!egalite) {
                        casesCouvertesParChaquePoint[point!!.id] = 1 + casesCouvertesParChaquePoint.getOrDefault(point!!.id,0)
                        board[""+i+"/"+j] = point!!.id
                    }
                    else {
                        board[""+i+"/"+j] = 0
                    }
                }
            }
        }

        return ""+nbSafes
    }

    data class Point(val x : Int, val y : Int, val id : Int)
}