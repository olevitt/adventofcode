import utils.Day
import utils.asResource
import utils.asResourceText

class Day2 : Day {

    override fun step1() : String {
        var sum2 = 0
        var sum3 = 0
        "/day2.txt".asResourceText().lines().forEach {
            var dic = mutableMapOf<Char,Int>()
            it.forEach {
                dic[it] = dic.getOrDefault(it,0) + 1
            }
            if (dic.containsValue(2)) {
                sum2++
            }
            if (dic.containsValue(3)) {
                sum3++
            }
        }
        return (sum2*sum3).toString()
    }

    override fun step2() : String {
        var input = "/day2.txt".asResourceText().lines()
        var match : String? = null
        input.find { word1 ->
            input.find() { word2 ->
                match = compare2words(word1,word2)
                match != null
            }
            match != null
        }

        return match!!
    }

    fun compare2words(word1 : String, word2 : String) : String? {
        var nbDifs = 0
        var diff = ""
        word1.forEachIndexed { index, c ->
            if (word2[index] != c) {
                nbDifs++
                diff = c.toString()
            }
        }
        if (nbDifs == 1) {
            return word1.replaceFirst(diff, "")
        }
        return null
    }
}