
import net.sourceforge.tess4j.Tesseract
import net.sourceforge.tess4j.util.LoadLibs
import utils.Day
import utils.asResourceText
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import net.sourceforge.tess4j.util.LoadLibs.extractTessResources
import java.awt.Graphics2D









class Day10 : Day {

    val pattern = Regex("position=<(.*),(.*)> velocity=<(.*),(.*)>")


    override fun step1() : String {

        var points = generatePoints()
        val inflection = findInflection(points)
        points.forEach {
            it.unMove()
        }

        val tessDataFolder = LoadLibs.extractTessResources("tessdata") // Extracts
        // Tessdata
        // folder
        // from
        // referenced
        // tess4j
        // jar
        // for
        // language
        // support

        // path
        var toOcr = printBoard(points)

        val tess = Tesseract()
        tess.setTessVariable("load_system_dawg ","F")
        tess.setTessVariable("load_freq_dawg ","F")
        tess.setTessVariable("load_punc_dawg ","F")
        tess.setTessVariable("load_number_dawg ","F")
        tess.setTessVariable("load_unambig_dawg ","F")
        tess.setTessVariable("load_bigram_dawg ","F")
        tess.setTessVariable("load_fixed_length_dawgs ","F")
        tess.setDatapath(tessDataFolder.getAbsolutePath()) // sets tessData

        return tess.doOCR(toOcr)
    }

    override fun step2(): String {
        val points = generatePoints()

        return ""+(findInflection(points)-1)
    }

    fun generatePoints() : MutableList<Point> {
        val points = mutableListOf<Point>()
        "/day10.txt".asResourceText().lines().forEach {
            val data = pattern.find(it)!!.groupValues
            val point = Point(data[1].trim().toInt(),data[2].trim().toInt(),data[3].trim().toInt(),data[4].trim().toInt())
            points.add(point)
        }
        return points
    }

    fun findInflection(points : MutableList<Point>) : Int {
        var distanceCourante = Integer.MAX_VALUE
        var i = 0
        while (true) {
            i++
            move(points)
            val nouvelleDistance = distanceTotale(points)
            if (nouvelleDistance > distanceCourante) {
                return i
            }
            distanceCourante = nouvelleDistance
        }
    }

    fun printBoard(points : MutableList<Point>) : File {
        val maxX = points.maxBy {
            it.x
        }!!.x

        val maxY = points.maxBy {
            it.y
        }!!.y

        val bi = BufferedImage(maxX+1, maxY+1, BufferedImage.TYPE_INT_ARGB)
        points.forEach {
            if (it.x >= 0 && it.y >= 0) {
                bi.setRGB(it.x,it.y, Color.RED.rgb)
            }

        }

        /*val FACTOR = 40f

        val scaleX = (bi.getWidth() * FACTOR).toInt()
        val scaleY = (bi.getHeight() * FACTOR).toInt()

        val buffered = BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_ARGB)

        val grph = buffered.graphics as Graphics2D
        grph.scale(FACTOR.toDouble(), FACTOR.toDouble())

        grph.drawImage(bi, 0, 0, null)
        grph.dispose()*/

        val toOcr = File.createTempFile("image",".png");
        ImageIO.write(bi, "PNG", toOcr);
        return toOcr
    }

    fun distanceTotale(points : MutableList<Point>) : Int {
        val maxX = points.maxBy {
            it.x
        }!!.x

        val maxY = points.maxBy {
            it.y
        }!!.y

        return maxX + maxY
    }

    fun move(points : MutableList<Point>) : MutableList<Point> {
        points.forEach {
            it.move()
        }
        return points
    }

    data class Point(var x : Int, var y : Int, var vx : Int, var vy : Int) {

        fun move() {
            x += vx
            y += vy
        }

        fun unMove() {
            x -= vx
            y -= vy
        }
    }



}