import utils.Day
import utils.asResource
import utils.asResourceText

class Day3 : Day {

    override fun step1() : String {
        var nbOverlappingSquares = 0
        val regex = Regex("#(.*) @ (.*),(.*): (.*)x(.*)")
        val board = hashMapOf<String,Int>()
        "/day3.txt".asResourceText().lines().forEach  {
            val matchResult = regex.find(it)
            val data = matchResult!!.groupValues //[#1 @ 7,589: 24x11, 1, 7, 589, 24, 11]
            for (i in Integer.parseInt(data[2]) until Integer.parseInt(data[2])+Integer.parseInt(data[4])) {
                for (j in Integer.parseInt(data[3]) until Integer.parseInt(data[3])+Integer.parseInt(data[5])) {
                    val coords = i.toString()+"/"+j.toString()
                    board.put(coords,board.getOrDefault(coords,0) + 1)
                }
            }
        }

        board.forEach {
            if (it.value >= 2) {
                nbOverlappingSquares++
            }
        }

        return nbOverlappingSquares.toString()
    }

    override fun step2(): String {
        val regex = Regex("#(.*) @ (.*),(.*): (.*)x(.*)")
        val board = hashMapOf<String,MutableList<String>>()
        var claims = hashSetOf<String>()
        "/day3.txt".asResourceText().lines().forEach  {
            val matchResult = regex.find(it)
            val data = matchResult!!.groupValues //[#1 @ 7,589: 24x11, 1, 7, 589, 24, 11]
            claims.add(data[1])
            for (i in Integer.parseInt(data[2]) until Integer.parseInt(data[2])+Integer.parseInt(data[4])) {
                for (j in Integer.parseInt(data[3]) until Integer.parseInt(data[3])+Integer.parseInt(data[5])) {
                    val coords = i.toString()+"/"+j.toString()
                    val currentDataInSquare = board[coords]
                    if (currentDataInSquare == null) {
                        board[coords] = mutableListOf(data[1])
                    }
                    else {
                        board[coords]?.add(data[1])
                        board[coords]?.forEach {
                            claims.remove(it)
                        }
                    }
                }
            }
        }

        return claims.first()
    }
}