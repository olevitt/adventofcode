import utils.Day
import utils.asResourceText


class Day7 : Day {

    val pattern = Regex("Step (.) must be finished before step (.) can begin")
    val numberOfLetters = 26

    override fun step1() : String {
        val prerequis = generatePrerequis()
        var result = ""
        var nextLetter = getNextWork(prerequis)
        while (nextLetter != null) {
            workDone(prerequis,nextLetter)
            result += nextLetter
            nextLetter = getNextWork(prerequis)
        }
        return result
    }

    override fun step2(): String {
        val numberOfWorkers = 5
        val prerequis = generatePrerequis()
        var workers = mutableListOf<Worker>()
        for (i in 0 until numberOfWorkers) {
            workers.add(Worker(prerequis))
        }
        var totalTime = 0
        var result = ""
        while (result.length < numberOfLetters) {

            workers.forEach {
                val nextLetter = it.doWork()
                if (nextLetter != null) {
                    result += nextLetter
                }
            }

            workers.forEach {
                it.takeWork()
            }
            totalTime++
        }

        return ""+(totalTime-1)
    }

    fun generatePrerequis() : MutableMap<String, MutableList<String>> {
        val prerequis = hashMapOf<String, MutableList<String>>()
        "/day7.txt".asResourceText().lines().forEach  {
            val values = pattern.find(it)!!.groupValues;
            prerequis[values[2]] = prerequis.getOrDefault(values[2], mutableListOf())
            prerequis[values[2]]!!.add(values[1])
        }

        for (char in 'A' until 'A'+numberOfLetters) {
            if (!prerequis.containsKey(char.toString())) {
                prerequis[char.toString()] = mutableListOf()
            }
        }

        return prerequis
    }

    class Worker constructor(val prerequis: MutableMap<String, MutableList<String>>) {
        val timeCost = 60
        var working = 0
        var workingLetter = ' '

        fun takeWork()  {
            if (working <= 0) {
                val nextLetter = getNextWork(prerequis)

                if (nextLetter == null) {
                    // println("Idle")
                    return
                }

                //println("Started working on $nextLetter")
                working = nextLetter[0] - 'A' + timeCost + 1
                workingLetter = nextLetter[0]
            }
        }

        fun doWork() : String? {
            working--
            if (working == 0) {
                //println("Finished working on $workingLetter")
                workDone(prerequis,""+workingLetter)
                return ""+workingLetter
            }
            else {
                return null
            }
        }

    }

    companion object {


        fun getNextWork(prerequis : MutableMap<String, MutableList<String>>) : String? {
            var result = ""
            var nextItem = prerequis.filter {
                it.value.size == 0
            }.minBy {
                it.key
            }

            if (nextItem == null) {
                return null
            }

            result += nextItem.key
            prerequis.remove(nextItem.key)

            return result
        }

        fun workDone(prerequis : MutableMap<String, MutableList<String>>, doneItem : String) {
            prerequis.forEach {
                it.value.remove(doneItem)
            }
        }
    }

}