import utils.Day
import utils.asResourceText
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.util.*


class Day4 : Day {

    val TYPE_WAKEUP = 0
    val TYPE_ASLEEP = 1
    val TYPE_BEGIN_SHIFT = 2
    val regexEvent = Regex("\\[(.*)\\] (.*)")
    val extractInt = Regex("Guard #(.*) begins shift")


    override fun step1() : String {

        val eventsList = parseInput()
        val analysis = analyse(eventsList)

        val result = analysis.totalAsleep.maxBy { it -> it.value }
        val bestMinute = analysis.minutesSpentAsleep[result!!.key]!!.maxBy { it -> it.value}!!

        return (result.key * bestMinute.key).toString()
    }

    override fun step2(): String {
        val eventsList = parseInput()
        val analysis = analyse(eventsList)

        var max = 0
        var idMax = 0
        var minuteMax = 0
        analysis.minutesSpentAsleep.forEach {
            val potentialMax = it.value.maxBy { it.value }!!
            if (potentialMax.value > max) {
                max = potentialMax.value
                minuteMax = potentialMax.key
                idMax = it.key
            }
        }

        return (idMax * minuteMax).toString()
    }

    fun analyse(eventsList : List<Event> ) : Analysis {
        var idGuard = 0
        var isAsleep : Event? = null
        val totalAsleep = hashMapOf<Int, Int>()
        val minutesSpentAsleep = hashMapOf<Int,HashMap<Int, Int>>()
        eventsList.forEach {
            if (it.action.type == TYPE_BEGIN_SHIFT) {
                idGuard = it.action.id
                isAsleep = null
            }
            if (it.action.type == TYPE_ASLEEP)
                isAsleep = it
            if (it.action.type == TYPE_WAKEUP) {
                val diff = Duration.between(isAsleep!!.timing, it.timing)
                val minutesTracking = minutesSpentAsleep.getOrDefault(idGuard, hashMapOf())

                for (minute in isAsleep!!.timing.minute until it.timing.minute) {
                    minutesTracking.put(minute,minutesTracking.getOrDefault(minute,0)+1)
                }

                minutesSpentAsleep[idGuard] = minutesTracking

                totalAsleep.put(idGuard,totalAsleep.getOrDefault(idGuard,0) +diff.toMinutes().toInt())
            }
        }

        return Analysis(totalAsleep, minutesSpentAsleep)
    }

    fun parseInput() : List<Event> {
        val eventsList = mutableListOf<Event>()
        val dateParser = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm", Locale.ENGLISH)
        "/day4.txt".asResourceText().lines().forEach  {
            val matchResult = regexEvent.find(it)
            val data = matchResult!!.groupValues //[#1 @ 7,589: 24x11, 1, 7, 589, 24, 11]
            val timing = getInstant(dateParser.parse(data[1]))
            var type = TYPE_ASLEEP
            var id = 0
            if (data[2].contains("wakes up")) {
                type = TYPE_WAKEUP
            }
            else if (data[2].contains("falls asleep")) {
                type = TYPE_ASLEEP
            }
            else {
                id = Integer.parseInt(extractInt.find(data[2])!!.groupValues[1])
                type = TYPE_BEGIN_SHIFT
            }

            eventsList.add(Event(timing, Action(type,id)))
        }

        eventsList.sortBy { event -> event.timing }
        return eventsList
    }

    fun getInstant(temporalAccessor : TemporalAccessor) : ZonedDateTime {
        var localDateTime = LocalDateTime.from(temporalAccessor)
        var zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.systemDefault())
        return zonedDateTime
    }


    data class Event(val timing : ZonedDateTime, val action : Action)
    data class Action(val type : Int, var id : Int)

    data class Analysis(val totalAsleep : Map<Int,Int>, val minutesSpentAsleep : Map<Int,HashMap<Int,Int>>)


}