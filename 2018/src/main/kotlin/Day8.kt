import utils.Day
import utils.asResourceText


class Day8 : Day {

    var currentIndex = 0
    var sumMetadata = 0

    override fun step1() : String {
        val input = "/day8.txt".asResourceText().split(" ")
        extractNode(input)
        return ""+sumMetadata
    }

    override fun step2(): String {
        val input = "/day8.txt".asResourceText().split(" ")
        val node = extractNode(input)

        return ""+node.nodeValue()
    }

    fun extractNode(input : List<String>) : Node {
        val node = Node()
        val nbNodes = Integer.parseInt(input[currentIndex])
        currentIndex++
        val nbData = Integer.parseInt(input[currentIndex])
        currentIndex++
        for (i in 0 until nbNodes) {
            node.childs.add(extractNode(input))
        }

        for (i in 0 until nbData) {
            node.metadata.add(Integer.parseInt(input[currentIndex]))
            currentIndex++
        }

        sumMetadata += node.metadata.sum()

        return node
    }





    data class Node(val metadata : MutableList<Int> = mutableListOf(),
                    val childs : MutableList<Node> = mutableListOf()) {
        fun nodeValue() : Int {
            if (childs.size == 0) {
                return metadata.sum()
            }

            return metadata.sumBy {
                var retour = 0
                if (it == 0 || it > childs.size) {
                    retour = 0
                }
                else {
                    retour = childs[it - 1].nodeValue()
                }
                retour
            }
        }
    }

}