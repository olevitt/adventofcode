import utils.Day
import utils.asResource
import utils.asResourceText

class Day1 : Day {

    override fun step1() : String {
        var sum = 0;
        "/day1.txt".asResourceText().lines().forEach {
            sum += Integer.parseInt(it)
        }
        return ""+sum
    }

    override fun step2() : String {
        var sum = 0
        var alreadySeen = hashSetOf(0)
        while ("/day1.txt".asResourceText().lines().find {
                alreadySeen.add(sum)
                sum += Integer.parseInt(it)
                alreadySeen.contains(sum)
            } == null) {

        }
        return ""+sum
    }
}