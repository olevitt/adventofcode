import utils.Day
import utils.asResourceText
import java.util.*


class Day9 : Day {
    val pattern = Regex("(.*) players; last marble is worth (.*) points")
    var nbPlayers = 0
    var lastMarble = 0


    override fun step1() : String {
        readData()
        return solve()
    }

    override fun step2(): String {
        readData()
        lastMarble = lastMarble * 100
        return solve()
    }

    class Node(val value : Int, var next : Node? = null, var previous : Node? = null) {

    }

    fun readData() {
        val data = pattern.find("/day9.txt".asResourceText())!!.groupValues
        nbPlayers = Integer.parseInt(data[1])
        lastMarble = Integer.parseInt(data[2])
    }

    fun solve() : String {
        var head = Node(value=0)
        val scores = Array(nbPlayers) { 0.toLong() }
        var currentPlayer = 0
        head.next = head
        head.previous = head
        var currentNode = head
        var currentMarble = 1
        while (currentMarble <= lastMarble) {
            if (currentMarble % 23 == 0) {
                scores[currentPlayer] = scores[currentPlayer] + currentMarble
                for (i in 0 until 6) {
                    currentNode = currentNode.previous!!
                }
                scores[currentPlayer] = scores[currentPlayer] + currentNode.previous!!.value
                currentNode.previous = currentNode.previous!!.previous
                currentNode.previous!!.next = currentNode

            }
            else {
                var newNode = Node(value=currentMarble)
                val nextNode = currentNode!!.next
                val nextNextNode = nextNode!!.next

                nextNode.next = newNode
                nextNextNode!!.previous = newNode

                newNode.previous = nextNode
                newNode.next = nextNextNode
                currentNode = newNode
            }


            currentMarble++

            currentPlayer++
            currentPlayer %= nbPlayers
        }

        return ""+scores.max()
    }

}