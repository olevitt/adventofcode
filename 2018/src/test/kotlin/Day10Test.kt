import org.junit.Assert
import org.junit.Test

class Day10Test {

    @Test
    fun step1() {
        Assert.assertEquals("KBJHEZCB",Day10().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("10369",Day10().step2())
    }

}