import org.junit.Assert
import org.junit.Test

class Day4Test {

    @Test
    fun step1() {
        Assert.assertEquals("119835",Day4().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("12725",Day4().step2())
    }

}