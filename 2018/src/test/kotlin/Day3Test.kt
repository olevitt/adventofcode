import org.junit.Assert
import org.junit.Test

class Day3Test {

    @Test
    fun step1() {
        Assert.assertEquals("117948",Day3().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("567",Day3().step2())
    }

}