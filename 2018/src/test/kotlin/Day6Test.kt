import org.junit.Assert
import org.junit.Test

class Day6Test {

    @Test
    fun step1() {
        Assert.assertEquals("4887",Day6().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("34096",Day6().step2())
    }

}