import org.junit.Assert
import org.junit.Test

class Day1Test {

    @Test
    fun step1() {
        Assert.assertEquals(Day1().step1(),"470")
    }

    @Test
    fun step2() {
        Assert.assertEquals(Day1().step2(),"790")
    }

}