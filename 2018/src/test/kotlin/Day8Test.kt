import org.junit.Assert
import org.junit.Test

class Day8Test {

    @Test
    fun step1() {
        Assert.assertEquals("43996",Day8().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("35189",Day8().step2())
    }

}