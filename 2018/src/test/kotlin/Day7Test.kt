import org.junit.Assert
import org.junit.Test

class Day7Test {

    @Test
    fun step1() {
        Assert.assertEquals("EFHLMTKQBWAPGIVXSZJRDUYONC",Day7().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("1056",Day7().step2())
    }

}