import org.junit.Assert
import org.junit.Test

class Day2Test {

    @Test
    fun step1() {
        Assert.assertEquals("7872",Day2().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("tjxmoewpdkyaihvrndfluwbzc",Day2().step2())
    }

}