import org.junit.Assert
import org.junit.Test

class Day5Test {

    @Test
    fun step1() {
        Assert.assertEquals("11252",Day5().step1())
    }

    @Test
    fun testEpure() {
        Assert.assertEquals("dabCBAcaDA",Day5().epure("dabAcCaCBAcCcaDA"))
        Assert.assertEquals("",Day5().epure("aA"))
        Assert.assertEquals("",Day5().epure("abBA"))
        Assert.assertEquals("aabAAB",Day5().epure("aabAAB"))
    }

    @Test
    fun step2() {
        Assert.assertEquals("6118",Day5().step2())
    }

}