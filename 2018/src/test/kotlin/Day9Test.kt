import org.junit.Assert
import org.junit.Test

class Day9Test {

    @Test
    fun step1() {
        Assert.assertEquals("412127",Day9().step1())
    }

    @Test
    fun step2() {
        Assert.assertEquals("3482394794",Day9().step2())
    }

}