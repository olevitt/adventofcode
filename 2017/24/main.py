# -*- coding: utf-8 -*-
import re
import math

leMax = 0
longueurMax = 0

def etape1():
    tuyaux = init()
        
    global leMax
    leMax = 0
    pont(0,0,tuyaux)
    return leMax

def init():
    fichier = open("input.txt","r")
    lines = fichier.read().splitlines()
    tuyaux = list()
    for line in lines:
        numbers = re.findall(r'\d+', line)
        tuyau = (int(numbers[0]),int(numbers[1]))
        tuyaux.append(tuyau)
    return tuyaux

def etape2():
    tuyaux = init()
        
    global leMax
    global longueurMax
    leMax = 0
    longueurMax = 0
    pont2(0,0,0,tuyaux)
    return leMax

def pont2(poids,longueur,nb,tuyaux):
    for tuyau in tuyaux:
        if (tuyau[0] == nb or tuyau[1] == nb):
            new_list = list(tuyaux)
            new_list.remove(tuyau)
            pont2(poids+tuyau[0]+tuyau[1],longueur + 1,tuyau[1] if tuyau[0] == nb else tuyau[0],new_list)
    
    global leMax
    global longueurMax
    if (longueur > longueurMax or (longueur == longueurMax and poids > leMax)):
        longueurMax = longueur
        leMax = poids
    
def pont(poids,nb,tuyaux):
    for tuyau in tuyaux:
        if (tuyau[0] == nb or tuyau[1] == nb):
            new_list = list(tuyaux)
            new_list.remove(tuyau)
            pont(poids+tuyau[0]+tuyau[1],tuyau[1] if tuyau[0] == nb else tuyau[0],new_list)
    
    global leMax
    if (poids > leMax):
        leMax = poids

print(etape1())
print(etape2())