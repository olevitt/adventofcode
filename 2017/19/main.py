# -*- coding: utf-8 -*-
import re

def etape1ET2():
    fichier = open("input.txt","r")
    lines = fichier.readlines()
    matrice = list()
    coords = (0,0)
    steps = 1
    for line in lines:
        matrice.append(line)
    
    coords = (0,matrice[0].index("|"))
    direction = (1,0)
    string = ""
    while (direction != None):
        while (getCharAt(matrice,fusionTuple(coords,direction)) != " "):
            coords = fusionTuple(coords,direction)
            char = getCharAt(matrice,coords)
            if (char >= "A" and char <= "Z"):
                string = string + char
            steps = steps + 1
        direction = trouverDirection(matrice,coords,direction)

    return (string,steps)

def trouverDirection(matrice,coords,direction):
    directions = [(0,1),(0,-1),(1,0),(-1,0)]
    
    for direc in directions:
        if (direc[0] == direction[0] or direc[1] == direction[1]):
            continue
        
        if (getCharAt(matrice,fusionTuple(coords,direc)) == " "):
            continue
        
        return direc
    return None

def getCharAt(matrice,coords):
    return matrice[coords[0]][coords[1]]

def fusionTuple(coords,direction):
    return (coords[0]+direction[0],coords[1]+direction[1])
    
result = etape1ET2()
print(result[0])
print(result[1])