# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:34:38 2017

@author: gon
"""

def etape1():
    file = open("input.txt","r")
    lines = file.readlines()
    total = 0
    for line in lines:
        data = line.split("\t")
        intData = []    
        for value in data:
            intData.append(int(value))                        
        dif = max(intData) - min(intData)
        total += dif
    print(total)
        
def etape2():
    file = open("input2.txt","r")
    lines = file.readlines()
    total = 0
    for line in lines:
        data = line.split("\t")
        intData = []    
        for value in data:
            intData.append(int(value))                        
        dif = findDivision(intData)
        total += dif
    print(int(total))

def findDivision(liste):
    for i in range(0,len(liste)):
        for j in range(i,len(liste)):
            if (i!=j and max(liste[i],liste[j]) % min(liste[i],liste[j]) == 0):
                return max(liste[i],liste[j]) / min(liste[i],liste[j])
                
etape1()
etape2()