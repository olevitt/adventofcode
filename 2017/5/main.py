# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:34:38 2017

@author: gon
"""

def etape1():
    file = open("input.txt","r")
    lines = file.readlines()
    data = []
    offset = 0
    nbIterations = 0
    for line in lines:
        data.append(int(line))
    i = 0
    while i < len(data):
        data[i] = data[i] + 1                
        i = i + data[i] - 1
        nbIterations = nbIterations + 1
    print(nbIterations)
        
def etape2():
    file = open("input.txt","r")
    lines = file.readlines()
    data = []
    offset = 0
    nbIterations = 0
    for line in lines:
        data.append(int(line))
    i = 0
    while i < len(data):
        offset = data[i]
        if (offset >= 3):
            data[i] = data[i] - 1
        else:
            data[i] = data[i] + 1             
        i = i + offset
        nbIterations = nbIterations + 1
    print(nbIterations)
                
etape1()
etape2()
