# -*- coding: utf-8 -*-
import re
import math


def etape1():
    universe = Universe()
    universe.go()
    nb = 0
    for key in universe.data:
        if (universe.data[key] == 1):
            nb = nb + 1
    
    return nb
    
def etape2():
    return 0
    
class Rule:
    
    def __init__(self):
        self.toto = 1

class Rules:
    
    def __init__(self):
        fichier = open("input.txt","r")
        lines = fichier.read().splitlines()
        patternDebut = re.compile("Begin in state ([A-Z]{1}).")
        patternDiagno = re.compile("Perform a diagnostic checksum after ([0-9]*) steps.")
        patternInState = re.compile("In state ([A-Z]{1}):")
        patternWrite = re.compile("- Write the value ([0-9]{1}).")
        patternMoveTo = re.compile("- Move one slot to the (right|left).")
        patternNextState = re.compile("- Continue with state ([A-Z]{1}).")
        begin = None
        nbIterations = 0
        rules = {}
        i = 0
        while i < len(lines):
            line = lines[i]
            match = patternDebut.match(line)
            if (match != None):
                begin = match.group(1)
            match = patternDiagno.match(line)
            if (match != None):
                nbIterations = int(match.group(1))
            match = patternInState.match(line)
            if (match != None):
                currentRuleName = match.group(1)
                currentRule = Rule()
                currentRule.value0 = int(patternWrite.search(lines[i+2]).group(1))
                currentRule.moveTo0 = 1 if patternMoveTo.search(lines[i+3]).group(1) == "right" else -1
                currentRule.nextState0 = patternNextState.search(lines[i+4]).group(1)
                
                currentRule.value1 = int(patternWrite.search(lines[i+6]).group(1))
                currentRule.moveTo1 = 1 if patternMoveTo.search(lines[i+7]).group(1) == "right" else -1
                currentRule.nextState1 = patternNextState.search(lines[i+8]).group(1)
                
                rules[currentRuleName] = currentRule
                
                i = i + 8
            
            i = i + 1
        self.nbIterations = nbIterations
        self.begin = begin
        self.rules = rules    
        
    def getSimpleRule(self,rule,state):
        laRule = self.rules[rule]
        if (state == 0):
            return (laRule.value0,laRule.moveTo0,laRule.nextState0)
        else:
            return (laRule.value1,laRule.moveTo1,laRule.nextState1)
        
class Universe:
    
    def __init__(self):
        self.rules = Rules()
        self.currentIteration = 0
        self.currentRule = self.rules.begin
        self.data = {}
        self.data[0] = 0
        self.position = 0
    
    def move(self):
        currentData = self.getCurrentData()
        self.currentIteration = self.currentIteration + 1
        instructions = self.rules.getSimpleRule(self.currentRule,currentData)
        self.data[self.position] = instructions[0]
        self.position = self.position + instructions[1]
        self.currentRule = instructions[2]
    
    def getCurrentData(self):
        if (self.position in self.data):
            return self.data[self.position]
        else:
            self.data[self.position] = 0
            return 0
            
    def go(self):
        for i in range(self.rules.nbIterations):
            self.move()                

print(etape1())
print(etape2())