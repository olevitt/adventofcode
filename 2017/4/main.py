# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:34:38 2017

@author: gon
"""

def etape1():
    file = open("input.txt","r")
    lines = file.readlines()
    total = 0
    for line in lines:
        data = line.split()
        dataVues = []
        ok = True
        for value in data:
            if (value in dataVues):
                ok = False
            dataVues.append(value)
        if (ok):
            total += 1
    print(total)
        
def etape2():
    file = open("input.txt","r")
    lines = file.readlines()
    total = 0
    for line in lines:
        data = line.split()
        dataVues = []
        ok = True
        for value in data:
            if (''.join(sorted(value)) in dataVues):
                ok = False
            dataVues.append(''.join(sorted(value)))
        if (ok):
            total += 1
    print(total)
                
etape1()
etape2()
