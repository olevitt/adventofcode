# -*- coding: utf-8 -*-
def etape1():
    file = open("input.txt","r")
    lines = file.readlines()
    ligne = lines[0]
    data = []
    for value in ligne.split("\t"):
        data.append(int(value))

    hashs = []
    h = to_hash(data)
    iterations = 0
    while h not in hashs:
        hashs.append(h)
        iterer(data)
        h = to_hash(data)
        iterations = iterations + 1
        
    return iterations
    

        
def etape2():
    file = open("input.txt","r")
    lines = file.readlines()
    ligne = lines[0]
    data = []
    for value in ligne.split("\t"):
        data.append(int(value))

    hashs = []
    h = to_hash(data)
    iterations = 0
    while h not in hashs:
        hashs.append(h)
        iterer(data)
        h = to_hash(data)
        iterations = iterations + 1
        
    return iterations - hashs.index(h)
   
def to_hash(data):
    h = ""
    for dat in data:
        h = h + " " + str(dat)
    return h
    
def iterer(data):
    max = -1
    index = -1    
    for i in range(len(data)):
        if (data[i] > max):
            max = data[i]
            index = i
    
    aRepartir = data[index]
    data[index] = 0
    while aRepartir > 0:
        index = (index + 1) % len(data)
        data[index] = data[index] + 1
        aRepartir = aRepartir - 1        

print(etape1())
print(etape2())
