# -*- coding: utf-8 -*-
import re


def etape1():
    input = "vbqugkhl";
    total = 0
    for i in range(128):
        aHasher = input + "-" + str(i)
        hash = toHash(aHasher)
        str2 = ""
        for carac in hash:
            str2 = str2 + str(hexToBinary(carac))
        for car in str2:
            if (car == "1"):
                total = total + 1
    return total
    
def etape2():
    input = "vbqugkhl";
    matrice = list(range(128))
    for i in range(128):
        matrice[i] = list(range(128))
        
    for i in range(128):
        aHasher = input + "-" + str(i)
        hash = toHash(aHasher)
        str2 = ""
        
        for carac in hash:
            str2 = str2 + str(hexToBinary(carac))
        
        for j in range(len(str2)):
            matrice[i][j] = int(str2[j])
            
    groupNumber = 0
    for i in range(128):
        for j in range(128):
            if (matrice[i][j] == 1):
                groupNumber = groupNumber + 1
                propagerChangement(matrice,i,j,groupNumber)
                
    return groupNumber
    
def propagerChangement(matrice,i,j,groupNumber):
    if (matrice[i][j] != 1):
        return
    matrice[i][j] = groupNumber * -1
    
    for (k,l) in [(-1,0),(1,0),(0,-1),(0,1)]:
        if (i+k < 0 or i+k > 127 or j+l < 0 or j+l > 127):
            continue
        if (matrice[i+k][j+l] == 1):
            propagerChangement(matrice,i+k,j+l,groupNumber)
            
    
def toHash(input):
    intData = list(map(ord,input))
    intData.extend([17, 31, 73, 47, 23])
    etat = status(256)
    for i in range(64):
        for data in intData:
            etat.move(data)
    return toHexa(etat.data)
    
def toHexa(data):
    string = ""
    for i in range(16):
        position = i * 16
        result = data[position]
        for j in range(1,16):
            result = result ^ data[position+j]
        hexResult = hex(result)
        if (len(hexResult) == 3):
            hexResult = hexResult[0] + hexResult[1] + "0" + hexResult[2]
        string = string + hexResult[2] + hexResult[3]
    return string 
    
def hexToBinary(hex):
    scale = 16 ## equals to hexadecimal

    num_of_bits = 4

    return bin(int(hex, scale))[2:].zfill(num_of_bits)

class status():
    data = 0
    position = 0
    skipIndex = 0
    size = 0
    
    def __init__(self,size):
        self.size = size
        self.data = list(range(size))
    
    def move(self,index):
        borneSup = self.position+index
        restant = 0
        if (borneSup > self.size-1):
            restant = borneSup - (self.size-1) - 1
            borneSup = self.size
        subList = self.data[self.position:borneSup]
        subList.extend(self.data[0:restant])
        subList.reverse()
        for i in range(index):            
            self.data[self.position] = subList[i]
            self.increasePosition(1)
        self.increasePosition(self.skipIndex)
        self.skipIndex = self.skipIndex + 1
    
    def increasePosition(self,howMuch):
        self.position = (self.position + howMuch) % self.size
        
    
print(etape1())
print(etape2())