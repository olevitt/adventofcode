# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:34:38 2017

@author: gon
"""


def findPremierInferieur(target):
    for i in range(1000):
        if (i%2 == 0):
            continue
        if (i*i >= target):
            return i - 2

def etape1(input):
    premier = findPremierInferieur(input)
    premierSuivant = premier + 2
    leMin = premier * premier    
    leMax = premierSuivant * premierSuivant
    cote = premierSuivant
    gap = int((cote - 1) / 2)
    centre = leMax
    maxDif = 1000000000000000
    for i in range(4):
        if (i == 0):
            centre = centre - gap
        else:    
            centre = centre - 2*gap
        dif = abs(input-centre)
        if (dif < maxDif):
            maxDif = dif
    
    return(maxDif + gap)


def etape2(input):
    matrice = []
    for i in range(1000):
        matrice.append([])
        for j in range(1000):
            matrice[i].append(0)
    
    i=0
    j=0
    matrice[i][j] = 1
    for couronne in range(1,1000000):
        i = i+1
        calcul(matrice,i,j)
        for x in range(0,2*couronne - 1):
            j = j+1
            if (calcul(matrice,i,j) > input):
                return matrice[i][j]
        for x in range(0,2*couronne):
            i = i-1
            if (calcul(matrice,i,j) > input):
                return matrice[i][j]
        for x in range(0,2*couronne):
            j = j-1
            if (calcul(matrice,i,j) > input):
                return matrice[i][j]
        for x in range(0,2*couronne):
            i = i+1
            if (calcul(matrice,i,j) > input):
                return matrice[i][j]

def calcul(matrice,i,j):
    if (matrice[i][j] != 0):
        return
    total = 0    
    for k in range(-1,2):
        for l in range(-1,2):
            total = total + matrice[i+k][j+l]            
    matrice[i][j] = total
    return total

print(etape1(325489))
print(etape2(325489))