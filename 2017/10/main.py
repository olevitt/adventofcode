# -*- coding: utf-8 -*-
import re

def etape1():
    stream = "63,144,180,149,1,255,167,84,125,65,188,0,2,254,229,24"
    intData = list(map(int, stream.split(",")))
    etat = status(256)
    for data in intData:
        etat.move(data)
    return(etat.data[0]*etat.data[1])
    
def etape2():
    stream = "63,144,180,149,1,255,167,84,125,65,188,0,2,254,229,24"
    intData = list(map(ord,stream))
    intData.extend([17, 31, 73, 47, 23])
    etat = status(256)
    for i in range(64):
        for data in intData:
            etat.move(data)
    return toHexa(etat.data)
    
def toHexa(data):
    string = ""
    for i in range(16):
        position = i * 16
        result = data[position]
        for j in range(1,16):
            result = result ^ data[position+j]
        hexResult = hex(result)
        if (len(hexResult) == 3):
            hexResult = hexResult[0] + hexResult[1] + "0" + hexResult[2]
        string = string + hexResult[2] + hexResult[3]
    return string 

class status():
    data = 0
    position = 0
    skipIndex = 0
    size = 0
    
    def __init__(self,size):
        self.size = size
        self.data = list(range(size))
    
    def move(self,index):
        borneSup = self.position+index
        restant = 0
        if (borneSup > self.size-1):
            restant = borneSup - (self.size-1) - 1
            borneSup = self.size
        subList = self.data[self.position:borneSup]
        subList.extend(self.data[0:restant])
        subList.reverse()
        for i in range(index):            
            self.data[self.position] = subList[i]
            self.increasePosition(1)
        self.increasePosition(self.skipIndex)
        self.skipIndex = self.skipIndex + 1
    
    def increasePosition(self,howMuch):
        self.position = (self.position + howMuch) % self.size
        
    
print(etape1())
print(etape2())
