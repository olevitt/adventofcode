# -*- coding: utf-8 -*-
import re

def etape1():
    file = open("input.txt","r")
    line = file.readline()
    deplacements = line.split(",")
    coords = [0,0,0]
    matrice = matriceDeplacements()
    for deplacement in deplacements:
        delta = matrice[deplacement]
        for i in range(3):
            coords[i] = coords[i] + delta[i]
            
    return maxCoord(coords)

def matriceDeplacements():
    matrice = {}
    matrice["n"] = [1,0,-1]
    matrice["s"] = [-1,0,1]
    matrice["ne"] = [0,1,-1]
    matrice["se"] = [-1,1,0]
    matrice["nw"] = [1,-1,0]
    matrice["sw"] = [0,-1,1]
    return matrice

def maxCoord(coords):
    return max(abs(coords[0]),abs(coords[1]),abs(coords[2]))
    
def etape2():
    file = open("input.txt","r")
    line = file.readline()
    deplacements = line.split(",")
    coords = [0,0,0]
    matrice = matriceDeplacements()
    maxVu = 0
    for deplacement in deplacements:
        delta = matrice[deplacement]
        for i in range(3):
            coords[i] = coords[i] + delta[i]
        maxVu = max(maxVu,maxCoord(coords))
            
    return maxVu

print(etape1())
print(etape2())
