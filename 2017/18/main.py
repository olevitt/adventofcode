# -*- coding: utf-8 -*-
import re

def etape1():
    registre = {}
    fichier = open("input.txt","r")
    ordres = fichier.readlines()
    regex = re.compile("([a-z]{3}) (-{0,1}[a-z0-9]{1}) {0,1}([-0-9a-z]*){0,1}")
    i = 0
    son = 0
    while (i < len(ordres)):
        ordre = ordres[i]
        match = regex.match(ordre)
        if (match.group(1) == "set"):
            if is_int(match.group(3)):
                registre[match.group(2)] = int(match.group(3))
            else:
                registre[match.group(2)] = getValeur(registre,match.group(3))
        if (match.group(1) == "add"):
            if is_int(match.group(3)):
                registre[match.group(2)] = getValeur(registre,match.group(2)) + int(match.group(3))
            else:
                registre[match.group(2)] = getValeur(registre,match.group(2)) + getValeur(registre,match.group(3))
        if (match.group(1) == "mul"):
            if is_int(match.group(3)):
                registre[match.group(2)] = getValeur(registre,match.group(2)) * int(match.group(3))
            else:
                registre[match.group(2)] = getValeur(registre,match.group(2)) * getValeur(registre,match.group(3))
        if (match.group(1) == "mod"):
            if is_int(match.group(3)):
                registre[match.group(2)] = getValeur(registre,match.group(2)) % int(match.group(3))
            else:
                registre[match.group(2)] = getValeur(registre,match.group(2)) % getValeur(registre,match.group(3))
        if (match.group(1) == "jgz"):
            if ((is_int(match.group(2)) and int(match.group(2)) > 0) or (not is_int(match.group(2)) and getValeur(registre,match.group(2)) > 0)):
                if is_int(match.group(3)):
                    i = i + int(match.group(3)) - 1
                else:
                    i = i + getValeur(registre,match.group(3)) - 1
        if (match.group(1) == "snd"):
            if is_int(match.group(2)):
                son = int(match.group(2))
            else:
                son = getValeur(registre,match.group(2))
        if (match.group(1) == "rcv"):
            if is_int(match.group(2)) and int(match.group(2)) > 0:
                return son
            elif getValeur(registre,match.group(2)) > 0:
                return son
        i = i + 1
    return 0

def is_int(s):
    try: 
        int(str(s))
        return True
    except ValueError:
        return False

def getValeur(registre,entree):
    print(entree)
    print(registre)
    if (entree in registre):
        return registre[entree]
    else:
        return 0

class registre:
    
    def __init__(self,buffer,buffer2,p):
        self.registre = {}
        self.registre["p"] = p
        fichier = open("input.txt","r")
        self.ordres = fichier.readlines()
        self.regex = re.compile("([a-z]{3}) (-{0,1}[a-z0-9]{1}) {0,1}([-0-9a-z]*){0,1}")
        self.i = 0
        self.buffer2 = buffer2
        self.buffer = buffer
        self.nb = 0
        
    def move(self):
        ordre = self.ordres[self.i]
        match = self.regex.match(ordre)
        if (match.group(1) == "set"):
            if is_int(match.group(3)):
                self.registre[match.group(2)] = int(match.group(3))
            else:
                self.registre[match.group(2)] = getValeur(self.registre,match.group(3))
        elif (match.group(1) == "add"):
            if is_int(match.group(3)):
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) + int(match.group(3))
            else:
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) + getValeur(self.registre,match.group(3))
        elif (match.group(1) == "mul"):
            if is_int(match.group(3)):
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) * int(match.group(3))
            else:
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) * getValeur(self.registre,match.group(3))
        elif (match.group(1) == "mod"):
            if is_int(match.group(3)):
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) % int(match.group(3))
            else:
                self.registre[match.group(2)] = getValeur(self.registre,match.group(2)) % getValeur(self.registre,match.group(3))
        elif (match.group(1) == "jgz"):
            if ((is_int(match.group(2)) and int(match.group(2)) > 0) or (not is_int(match.group(2)) and getValeur(self.registre,match.group(2)) > 0)):
                if is_int(match.group(3)):
                    self.i = self.i + int(match.group(3)) - 1
                else:
                    self.i = self.i + getValeur(self.registre,match.group(3)) - 1
        elif (match.group(1) == "snd"):
            if is_int(match.group(2)):
                self.buffer2.append(int(match.group(2)))
            else:
                self.buffer2.append(getValeur(self.registre,match.group(2)))
            self.nb = self.nb + 1
        elif (match.group(1) == "rcv"):
            if (len(self.buffer) > 0):
                self.registre[match.group(2)] = self.buffer.pop(0)
            else:
                return False
        self.i = self.i + 1
        if (self.i >= len(self.ordres)):
            return False
        else: 
            return True

def etape2():    
    list1 = list()
    list2 = list()
    registre1 = registre(list1,list2,0)
    registre2 = registre(list2,list1,1)
    i = 0
    for i in range(0,1000):
        while (registre1.move()):
            i = i + 1
        while (registre2.move()):
            i = i + 1
    return registre2.nb
    
#print(etape1())
print(etape2())