# -*- coding: utf-8 -*-
import re

def generateList(valinput,iterations):
    array = [0]
    position = 0
    for i in range(iterations):
        position = position + valinput
        position = position % (i + 1)
        array.insert(position+1,i+1)
        position = position + 1
    return array
    
def etape1():
    valinput = 316
    iterations = 2017
    result = generateList(valinput,iterations)
    index = result.index(2017)
    return result[(index+1) % len(result)]

def etape2():
    valinput = 316
    iterations = 50000000
    position = 0
    valeurApresZero = 0
    for i in range(iterations):
        position = position + valinput
        position = position % (i + 1)
        if (position == 0):
            valeurApresZero = i + 1
        position = position + 1
        
    return valeurApresZero
    
print(etape1())
print(etape2())