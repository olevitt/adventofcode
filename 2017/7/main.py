# -*- coding: utf-8 -*-
import re

def etape1():
    graph = {}
    poids = {}
    entry = buildGraph(graph,poids)
    return entry
    

        
def etape2():
    graph = {}
    poids = {}
    entry = buildGraph(graph,poids)
    
    return calculPoidsPorte(graph,entry,poids)

def calculPoidsPorte(graph,entry,poids,problemes = []):
    if len(graph[entry]) == 0:
        return poids[entry]
    
    poidsPrecedent = 0
    poidsTotal = 0
    for subNode in graph[entry]:
        poidsSubNode = calculPoidsPorte(graph,subNode,poids,problemes)
        if (poidsPrecedent == 0):
            poidsPrecedent = poidsSubNode
            
        if (poidsSubNode != poidsPrecedent):
            dif = poidsPrecedent - poidsSubNode
            print("Redressage de "+str(dif))
            print(subNode+ " passe de "+str(poids[subNode])+" à "+str(poids[subNode]+dif))
            poids[subNode] = (poids[subNode]+dif)
            poidsSubNode = poidsSubNode + dif

        poidsTotal = poidsTotal + poidsSubNode

    return poids[entry] + poidsTotal

        

#Construit le graph et renvoi sont point d'entrée
def buildGraph(graph,poids):
    file = open("input.txt","r")
    lines = file.readlines()
    patternMots = re.compile("[a-z]+")
    patternChiffres = re.compile("[0-9]+")
    soutenuPar = {}
    for line in lines:
        mots = patternMots.findall(line)
        poidsCourant = patternChiffres.findall(line)
        currentNode = mots[0]
        for voisin in mots[1:len(mots)]:
            soutenuPar[voisin] = currentNode
        graph[currentNode] = mots[1:len(mots)]
        poids[currentNode] = int(poidsCourant[0])
    for key in graph:
        if not key in soutenuPar:
            return key
   
def to_hash(data):
    h = ""
    for dat in data:
        h = h + " " + str(dat)
    return h
    
def iterer(data):
    max = -1
    index = -1    
    for i in range(len(data)):
        if (data[i] > max):
            max = data[i]
            index = i
    
    aRepartir = data[index]
    data[index] = 0
    while aRepartir > 0:
        index = (index + 1) % len(data)
        data[index] = data[index] + 1
        aRepartir = aRepartir - 1        

#print(etape1())
print(etape2())
