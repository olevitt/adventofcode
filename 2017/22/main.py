# -*- coding: utf-8 -*-
import re
import math

def etape1():
    universe = Universe()
    virus = Virus()
    virus.pos = universe.centre()
    nbInfections = 0
    for i in range(10000):
        nbInfections = nbInfections + virus.move(universe)
        
    return nbInfections
    
def etape2():
    universe = Universe()
    virus = Virus2()
    virus.pos = universe.centre()
    nbInfections = 0
    for i in range(10000000):
        nbInfections = nbInfections + virus.move(universe)
        
    return nbInfections
    
class Universe:
    def __init__(self):
        fichier = open("input.txt","r")
        lines = fichier.readlines()
        self.universe = {}
        self.leCentre = (int(len(lines) / 2),int(((len(lines[0]) - 1) / 2)))
        i = 0
        j = 0
        for i in range(len(lines)):
            line = lines[i]
            for j in range(len(line) - 1):
                char = line[j]
                self.universe[(j,i)] = char
                
    def centre(self):
        return self.leCentre
        
    def getStatus(self,pos):
        if pos in self.universe:
            return self.universe[pos]
        else:
            self.universe[pos] = "."
            return "."
    
    def repr(self):
        for i in range(-10,10):
            for j in range(-10,10):
                print(self.getStatus((j,i)),end='')
            print()
        

class Virus:
    
    ordre = [(0,1),(-1,0),(0,-1),(1,0)]    
    
    def __init__(self):
        self.pos = (0,0)
        self.orientation = (0,-1)
        
    def move(self, universe):
        status = universe.getStatus(self.pos)
        infection = self.isInfection(status)
        self.turn(status)
        universe.universe[self.pos] = self.circleOfLife(status)
        self.forward()
        return 1 if infection else 0
        
    def isInfection(self,status):
        return status == '.'
        
    def forward(self):
        self.pos = (self.pos[0] + self.orientation[0],self.pos[1] + self.orientation[1])
        
    
    def turn(self,status):
        current = self.ordre.index(self.orientation)
        nextSens = self.ordre[(current +  self.turnOfLife(status)) % len(self.ordre)]
        self.orientation = nextSens
        
    def turnOfLife(self,status):
        if (status == '.'):
            return -1
        else:
            return 1
        
    def circleOfLife(self,previous):
        if previous == '.':
            return '#'
        elif previous == '#':
            return '.'
        return '.'
        
class Virus2(Virus):
    
    def turnOfLife(self,status):
        if (status == '.'):
            return -1
        if (status == '#'):
            return 1
        if (status == 'W'):
            return 0
        return 2
        
    def circleOfLife(self,previous):
        if previous == '.':
            return 'W'
        elif previous == 'W':
            return '#'
        elif previous == '#':
            return 'F'
        elif previous == 'F':
            return '.'
    
    def isInfection(self,status):
        return status == 'W'

print(etape1())
print(etape2())