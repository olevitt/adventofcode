# -*- coding: utf-8 -*-
import re

def etape1():
    file = open("input.txt","r")
    stream = file.readline()
    return calculTotal(stream)
    
def etape2():
    file = open("input.txt","r")
    stream = file.readline()
    return calculGarbage(stream)
    

def calculTotal(stream):
    depth = 0
    shouldSkip = False
    inGarbage = False
    total = 0
    for char in stream:
        if (shouldSkip):
            shouldSkip = False
            continue
        
        if (char == "!"):
            shouldSkip = True
            
        if (inGarbage):
            if (char == ">"):
                inGarbage = False
            continue
        
        if (char == "<"):
            inGarbage = True
        
        
        
        if (char == "{"):
            depth = depth + 1
        
        if (char == "}"):           
            total = total + depth
            depth = depth - 1
    
            
    
    return total
    
def calculGarbage(stream):
    depth = 0
    shouldSkip = False
    inGarbage = False
    total = 0
    garbage = 0
    for char in stream:
        if (shouldSkip):
            shouldSkip = False
            continue
        
        if (char == "!"):
            shouldSkip = True
            continue
            
        if (inGarbage):
            if (char == ">"):
                inGarbage = False
            else: 
                garbage = garbage + 1
            continue
        
        if (char == "<"):
            inGarbage = True
        
        
        
        if (char == "{"):
            depth = depth + 1
        
        if (char == "}"):           
            total = total + depth
            depth = depth - 1
    
            
    
    return garbage
    
print(etape1())
print(etape2())
