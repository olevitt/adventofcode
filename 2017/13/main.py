# -*- coding: utf-8 -*-
import re

def etape1():
    
    return tryTrip(28)
    
def etape2():
    file = open("input.txt","r")
    lines = file.readlines()
    layers = {}
    pattern = re.compile("([0-9]+)")
    maxPosition = 0
    for line in lines:
        m = pattern.findall(line)
        layers[int(m[0])] = layer(int(m[1]))
        maxPosition = max(int(m[0]),maxPosition)
        
    for delay in range(1400000000):
        if(testDelay(delay,layers)):
            return delay
            
    return 0
    
def testDelay(delay, layers):
    for depth in layers:
        if ((delay+depth)%(2*(layers[depth].layerRange - 1)) == 0):
            return False
    return True
        
    
def tryTrip(delay):
    file = open("input.txt","r")
    lines = file.readlines()
    layers = {}
    pattern = re.compile("([0-9]+)")
    maxPosition = 0
    for line in lines:
        m = pattern.findall(line)
        layers[int(m[0])] = layer(int(m[1]))
        maxPosition = max(int(m[0]),maxPosition)
    
    return severityOfTrip(layers,maxPosition,delay * -1 - 1)
      
    
def severityOfTrip(layers,maxPosition,startPosition):
    position = startPosition
    totalSeverity = 0
    for i in range(0,maxPosition+1 - startPosition):
        position = position + 1
        if (position in layers):
            if (layers[position].scannerPosition == 0):
                severity = layers[position].layerRange * position
                totalSeverity = totalSeverity + severity
        
        for depth in layers:
            layers[depth].move()
    return totalSeverity

class layer():
    scannerPosition = 0
    layerRange = 0
    sens = 1
    
    def __init__(self,layerRange):
        self.layerRange = layerRange
    
    def move(self):
        if (self.scannerPosition >= self.layerRange - 1):
            self.sens = -1
        elif (self.scannerPosition <= 0):
            self.sens = 1
        self.scannerPosition = self.scannerPosition + self.sens
        
    
print(etape1())
print(etape2())
