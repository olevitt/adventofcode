# -*- coding: utf-8 -*-
import re

def etape1():
    file = open("input.txt","r")
    lines = file.readlines()
    pattern = re.compile("([a-z]*) (dec|inc) (-?[0-9]*) if ([a-z]*) (<=|>=|>|<|==|!=) (-?[0-9]*)")
    values = {}
    for line in lines:
        print(line)
        m = pattern.match(line)
        
        key = m.group(1)
        dif = int(m.group(3))
        if (m.group(2) == "dec"):
            dif = dif * (-1)
        
        if (key not in values):
            values[key] = 0
        
        val = 0
        if (m.group(4) in values):
            val = values[m.group(4)]
        
        if (eval(str(val)+" "+m.group(5)+" "+m.group(6))):
            values[key] = values[key] + dif
        
    
        
    
    return max(values.values())
    
def etape2():
    file = open("input.txt","r")
    lines = file.readlines()
    pattern = re.compile("([a-z]*) (dec|inc) (-?[0-9]*) if ([a-z]*) (<=|>=|>|<|==|!=) (-?[0-9]*)")
    values = {}
    currentMax = 0
    for line in lines:
        print(line)
        m = pattern.match(line)
        
        key = m.group(1)
        dif = int(m.group(3))
        if (m.group(2) == "dec"):
            dif = dif * (-1)
        
        if (key not in values):
            values[key] = 0
        
        val = 0
        if (m.group(4) in values):
            val = values[m.group(4)]
        
        if (eval(str(val)+" "+m.group(5)+" "+m.group(6))):
            values[key] = values[key] + dif
        
        if (values[key] > currentMax):
            currentMax = values[key]
    
        
    
    return currentMax

#print(etape1())
print(etape2())
