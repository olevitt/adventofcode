# -*- coding: utf-8 -*-
import re

def etape1():
    graph = generateGraph()
    sett = set()
    return analyseLinks(graph,0,sett)
    
    
def analyseLinks(graph,numero, sett):
    for link in graph[numero]:
        if (link not in sett):
            sett.add(link)
            newLinks = analyseLinks(graph, link, sett)
            if (len(newLinks) > 0):
                sett.update(newLinks)
    return sett

def generateGraph():
    file = open("input.txt","r")
    lines = file.readlines()
    data = {}
    pattern = re.compile("([0-9]+)")
    for line in lines:
        m = pattern.findall(line)    
        current = int(m[0])
        data[current] = [];
        for link in m[1::]:
            linkedTo = int(link)
            data[current].append(linkedTo)
        
    return data
    
def etape2():
    graph = generateGraph()
    
    groupes = []
    for i in range(len(graph)):
        dejaGroupe = False
        for group in groupes:
            if (i in group):
                dejaGroupe = True
        if (not dejaGroupe):
            sett = set()
            groupes.append(analyseLinks(graph,i,sett))
    return groupes

print(len(etape1()))
print(len(etape2()))
