# -*- coding: utf-8 -*-

import re

def etape1():
    file = open("input.txt","r")
    line = file.readline()
    ordres = line.split(",")
    group = list("abcdefghijklmnop")
    return letsDance(group,ordres,re.compile("([0-9]+)"))
    
    
def letsDance(group,ordres,pattern):
    for ordre in ordres:
        if (ordre[0] == "s"):
            howMuch = int(pattern.findall(ordre)[0])
            group = group[(len(group)-howMuch):] +  group[0:(len(group)-howMuch)]
        elif (ordre[0] == "x"):
            howMuch = pattern.findall(ordre)
            swap(group,int(howMuch[0]),int(howMuch[1]))
        elif (ordre[0] == "p"):
            who = ordre[1]
            index1 = group.index(who)
            who2 = ordre[3]
            index2 = group.index(who2)
            swap(group,index1,index2)
    return group
    
def swap(group,index1,index2):
    temp = group[index1]
    group[index1] = group[index2]
    group[index2] = temp
    return group

def etape2():
    pattern = re.compile("([0-9]+)")
    file = open("input.txt","r")
    line = file.readline()
    ordres = line.split(",")
    group = list("abcdefghijklmnop")
    
    invariance = findInvariance(group,ordres)
    
    group = list("abcdefghijklmnop")

    group = letsDance(group,ordres,pattern) #Etape 1
    resteAFaire = (1000 * 1000 * 1000 - 1) % invariance
    
    for i in range(0,resteAFaire):
        group = letsDance(group,ordres,pattern)

    return group
    
def findInvariance(group,ordres):
    pattern = re.compile("([0-9]+)")
    dejavus = []
    for i in range(1000*1000):
        group = letsdance(group,ordres,pattern)
        leHash = "".join(group)
        if (leHash in dejavus):
            return i
        dejavus.append(leHash)
    
        
    
print("".join(etape1()))
print("".join(etape2()))