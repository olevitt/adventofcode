# -*- coding: utf-8 -*-

def etape1():
    a = 591
    b = 393
    modulo = 65536
    total = 0
    for i in range(40*1000*1000):
        a = (a * 16807) % 2147483647
        b = (b * 48271) % 2147483647
        if (a % modulo == b % modulo):
            total = total + 1
    return total

def etape2():
    a = 591
    b = 393
    modulo = 65536
    total = 0
    i = 0
    while (i < 5 * 1000 * 1000):
        a = (a * 16807) % 2147483647
        b = (b * 48271) % 2147483647
        while (a % 4 != 0):
            a = (a * 16807) % 2147483647
        while (b % 8 != 0):
            b = (b * 48271) % 2147483647
        i = i + 1
        if (a % modulo == b % modulo):
            total = total + 1
    return total
        
    
print(etape1())
print(etape2())