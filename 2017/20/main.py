# -*- coding: utf-8 -*-
import re
import math

def etape1():
    fichier = open("input.txt","r")
    lines = fichier.readlines()
    leMin = findMinAccel(lines)    
    return leMin

def findMinAccel(lines):
    leMin = 999999
    i = 0
    minPrecedent = 0
    for line in lines:
        numbers = re.findall(r'-?\d+', line)
        position = numbers[0:3]
        vitesse = numbers[3:6]
        accel = numbers[6:9]
        totalAccel = distance(accel)
        if (totalAccel < leMin):
            leMin = totalAccel
            minPrecedent = i
        elif (totalAccel == leMin):
            result = comparer(lines,i,minPrecedent)
            if (result > 0):
                minPrecedent = i
            
        i = i + 1
   
    return minPrecedent

def comparer(lines,i,j):
    line1 = lines[i]
    line2 = lines[j]
    numbers1 = re.findall(r'-?\d+', line1)
    numbers2 = re.findall(r'-?\d+', line2)
    accel1 = numbers1[6:9]
    accel2 = numbers2[6:9]
    distance1 = distance(accel1)
    distance2 = distance(accel2)
    if (distance1 != distance2):
        return distance1 - distance2
    
    vitesse1 = list()
    vitesse2 = list()
    for i in range(3,6):
        vitesse1.append(int(numbers1[i]) * sign(int(numbers1[i+3])))
        vitesse2.append(int(numbers2[i]) * sign(int(numbers2[i+3])))
        
    ecartVitesse = comparerDistances(vitesse1,vitesse2)
    
    if (ecartVitesse != 0):
        return ecartVitesse
    
    position1 = list()
    position2 = list()
    for i in range(3,6):
        position1.append(int(numbers1[i]) * sign(int(numbers1[i+6])))
        position2.append(int(numbers2[i]) * sign(int(numbers2[i+6])))
        
    ecartPosition = comparerDistances(position1,position2)
    
    return ecartPosition

def comparerDistances(vecteur1,vecteur2):
    distance1 = distance(vecteur1)
    distance2 = distance(vecteur2)
    return distance1 - distance2

def distance(vecteur):
    return math.fabs(int(vecteur[0])) + math.fabs(int(vecteur[1])) + math.fabs(int(vecteur[2]))

sign = lambda x: (1, -1)[x < 0]

class Point:
    
    def __init__(self, numbers):
        self.position = list(map(int,numbers[0:3]))
        self.vitesse = list(map(int,numbers[3:6]))
        self.accel = list(map(int,numbers[6:9]))
        
    def move(self):
        self.vitesse = [x + y for x, y in zip(self.vitesse, self.accel)]
        self.position = [x + y for x, y in zip(self.vitesse, self.position)]
        

    
    def __repr__(self):
        return str(self.position)

def etape2():
    fichier = open("input.txt","r")
    lines = fichier.readlines()
    points = list()
    for line in lines:
        numbers = re.findall(r'-?\d+', line)
        point = Point(numbers)
        points.append(point)
    
    print(points)
    
    for point in points:
        point.move()
        
    print(points)
    points.sort(key=keyfunction)
    print(points)
    
def keyfunction(item):
    return item.position

print(etape2())